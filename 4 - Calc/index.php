<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Калькулятор</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form action="" method="post">
        <div class="row">
            <input class="col-4 equation" type="text" name="equation" value="">
        </div>
        <div class="row">
            <button type="button" class="col-1 but">1</button>
            <button type="button" class="col-1 but">2</button>
            <button type="button" class="col-1 but">3</button>
            <button type="button" class="col-1 but">+</button>
        </div>
        <div class="row">
            <button type="button" class="col-1 but">4</button>
            <button type="button" class="col-1 but">5</button>
            <button type="button" class="col-1 but">6</button>
            <button type="button" class="col-1 but">-</button>
        </div>
        <div class="row">
            <button type="button" class="col-1 but">7</button>
            <button type="button" class="col-1 but">8</button>
            <button type="button" class="col-1 but">9</button>
            <button type="button" class="col-1 but">*</button>
        </div>
        <div class="row">
            <button type="button" class="col-1 but">0</button>
            <button type="button" class="col-1 but">(</button>
            <button type="button" class="col-1 but">)</button>
            <button type="button" class="col-1 but">/</button>
        </div>
        <div class="row">
            <button class="col-1" type="submit">=</button>
            <button class="col-1" type="reset">Reset</button>
        </div> 
        <div class="row">
        <?php
            if (!empty($_POST['equation'])) {
                $s=$_POST['equation'];
                eval("\$prim=".$s.";");
                echo $prim;
            }
            ?>
        </div>
        </form> 
    </div>
    <script src="script.js"></script>
</body>
</html>